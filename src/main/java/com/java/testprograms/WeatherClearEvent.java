package com.java.testprograms;

public class WeatherClearEvent implements Event {

	@Override
	public void applyEvent() {
		if(AutoUtils.appliedProps.containsKey(AutoUtils.WEATHER_RAINY_TYPE))
		{
			AutoUtils.currentSpeed += AutoUtils.WEATHER_RAINY_SPEED;
			AutoUtils.appliedProps.remove(AutoUtils.WEATHER_RAINY_TYPE);
		}		
	}
}

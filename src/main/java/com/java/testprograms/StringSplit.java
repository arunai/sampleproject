package com.java.testprograms;

public class StringSplit {
	String str= "This is example for split using dellimeter";
	
	public void StringSplit() {
		String[] result =str.split(" ");
		for(String s :result) {
			System.out.println(s);
		}
	}
	
	public void StrInitialition() {
		String str1 = "This is string";
		String str2 = new String("This is ");
		char[] c = {'a','b','c','d'};
		String str3 = new String(c);
		if(str1.equals(str2)) {
			System.out.println("true");
		}
		else
		{
			System.out.println("false");
		}
		System.out.println(str3);
	}
	public static void main(String[] args) {
		StringSplit obj = new StringSplit();
		obj.StringSplit();
		StringSplit obj1 = new StringSplit();
		obj1.StrInitialition();
	}

}

package com.java.testprograms;

public class SpeedLimitSignEvent implements Event {

	int inputEvent;
	public SpeedLimitSignEvent(int inputEvent)
	{
		this.inputEvent = inputEvent;
	}
	@Override
	public void applyEvent() {
		//Remove emergency turbo speed before applying posted speed limit.
		if(AutoUtils.appliedProps.containsKey(AutoUtils.EMERGENCY_TURBO_TYPE))
		{
			AutoUtils.currentSpeed -= AutoUtils.EMERGENCY_TURBO_SPEED;
		}
		if(AutoUtils.appliedProps.containsKey(AutoUtils.TRAFFIC_TYPE))
		{
			inputEvent -= AutoUtils.TRAFFIC_SPEED;
		}
		if(AutoUtils.appliedProps.containsKey(AutoUtils.WEATHER_RAINY_TYPE))
		{
			inputEvent -= AutoUtils.WEATHER_RAINY_SPEED;
		}
		if(AutoUtils.appliedProps.containsKey(AutoUtils.SLIPPERY_ROAD_TYPE))
		{
			inputEvent -= AutoUtils.SPLIPPERY_RD_SPEED;
		}
		if(AutoUtils.SPORT.equals(AutoUtils.mode))
			inputEvent += AutoUtils.SPEED_LIMIT_SIGN;
		else if(AutoUtils.SAFE.equals(AutoUtils.mode))
			inputEvent -= AutoUtils.SPEED_LIMIT_SIGN;
			
		AutoUtils.currentSpeed = inputEvent;
		AutoUtils.appliedProps.put(AutoUtils.SPEED_LIMIT_SIGN,inputEvent);		
	}

}

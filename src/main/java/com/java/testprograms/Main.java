package com.java.testprograms;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        String mode = "Normal";
        Boolean traffic = false;
        Boolean trafficClear = true;
        Boolean weatherRainy = false;
        Boolean weatherClear;
        Boolean slipperyRoad = false;
        Boolean slipperyRoadClear;
        Boolean emergencyTurbo = false;
        Boolean speedLimitSignX;
        int event = 0;
        mode = "Normal";
        int speed = 20;
        System.out.println("Please give event number");
        Scanner s = new Scanner(System.in);
        event = s.nextInt();
        int requitedSpeed = event;
        int inputtedSpeed = 0;
        event = (event >= 10) && (event <= 100) ? 101 : event;
        int newSpeed = (event >= 10) || (event <= 100) ? requitedSpeed : 0;


        while (event > 0) {
            switch (mode) {

                case "Normal":
                    switch (event) {
                        case 1:
                            if (!traffic) {
                                if (speed > 10) {
                                    speed = speed - 10;
                                    if (speed<10) {
                                        speed = 10;
                                    }
                                    System.out.println("your speed is: " + speed);
                                } else {
                                    speed = 10;
                                    System.out.println("your speed is: " + speed);
                                }
                                trafficClear = false;
                                traffic = true;
                            } else {
                                System.out.println("There's already traffic");
                                System.out.println("your speed is: " + speed);
                            }
                            break;
                        case 2:
                            if (!traffic) {
                                System.out.println("There's no traffic to clear");
                                System.out.println("your speed is: " + speed);
                            } else {
                                speed = speed + 10;
                                System.out.println("your speed is: " + speed);
                                trafficClear = true;
                                traffic = false;
                            }
                            break;
                        case 3:
                            if(!weatherRainy) {
                                if (speed > 10) {
                                    speed = speed - 5;
                                    if (speed<10) {
                                        speed = 10;
                                    }
                                    System.out.println("your speed is: " + speed);
                                } else {
                                    speed = 10;
                                    System.out.println("your speed is: " + speed);
                                }
                                weatherClear = false;
                                weatherRainy = true;
                            } else {
                                System.out.println("weather is already rainy");
                                System.out.println("your speed is: " + speed);
                            }
                            break;
                        case 4:
                            if (!weatherRainy) {
                                System.out.println("Weather is already clear");
                                System.out.println("your speed is: " + speed);
                            } else {
                                speed = speed + 5;
                                System.out.println("your speed is: " + speed);
                                weatherClear = true;
                                weatherRainy = false;
                            }
                            break;
                        case 5:
                            if (!slipperyRoad) {
                                if (speed > 10) {
                                    speed = speed - 15;
                                    if (speed<10) {
                                        speed = 10;
                                    }
                                    System.out.println("your speed is: " + speed);
                                } else {
                                    speed = 10;
                                    System.out.println("your speed is: " + speed);
                                }
                                slipperyRoadClear = false;
                                slipperyRoad = true;
                            } else {
                                System.out.println("Road is already slippery");
                                System.out.println("your speed is: " + speed);
                            }
                            break;
                        case 6:
                            if (!slipperyRoad) {
                                System.out.println("There's no slippery road");
                                System.out.println("your speed is: " + speed);
                            } else {
                                speed = speed + 15;
                                System.out.println("your speed is: " + speed);
                                slipperyRoadClear = true;
                                slipperyRoad = false;
                            }
                            break;
                        case 7:
                            if (slipperyRoad && emergencyTurbo) {
                                System.out.println("Emergency Turbo can't be applied when road is slippery or already in turbo mode");
                                System.out.println("your speed is: " + speed);
                            } else {
                                speed = speed + 20;
                                System.out.println("your speed is: " + speed);
                                slipperyRoadClear = true;
                                slipperyRoad = false;
                                emergencyTurbo = true;
                            }
                            break;
                        case 101:
                            emergencyTurbo = false;
                            speed = newSpeed;
                            System.out.println("your speed is: " + speed);
                            break;
                    }
                    System.out.println("Please give event number");
                    Scanner s1 = new Scanner(System.in);
                    event = s1.nextInt();
                    requitedSpeed = event;
                    inputtedSpeed = 0;
                    event = (event >= 10) && (event <= 100) ? 101 : event;
                    newSpeed = (event >= 10) || (event <= 100) ? requitedSpeed : 0;

            }
        }
    }
}

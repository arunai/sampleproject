package com.java.testprograms;

import java.util.Properties;

public class AutoUtils {

	public static final int TRAFFIC_TYPE = 1 , TRAFFIC_CLEAR_TYPE = 2, WEATHER_RAINY_TYPE = 3, WEATHER_CLEAR_TYPE = 4, SLIPPERY_ROAD_TYPE = 5, SLIPPERY_ROAD_CLEAR_TYPE = 6, EMERGENCY_TURBO_TYPE = 7;
	public static int INITIAL_SPEED = 20, MIN_SPEED_LIMIT = 10, MAX_SPEED_LIMIT = 100, TRAFFIC_SPEED = 10, WEATHER_RAINY_SPEED = 5, SPLIPPERY_RD_SPEED = 15, EMERGENCY_TURBO_SPEED = 20, SPEED_LIMIT_SIGN = 10;
	public static final String ERROR_INVALID_EVENT = "Invalid Event !" , WELCOME_MSG = "Welcome. Driving mode is ", SPEED_IS = "Speed is ", ENTER_EVENT = "Enter Event :" , DEFAULT_NORMAL_MODE = "NORMAL",SPORT = "Sport",SAFE ="Safe";
	public static int noOfTimesTrafficApplied;
	public static Properties appliedProps = new Properties();
	public static int currentSpeed = INITIAL_SPEED;
	public static String mode = DEFAULT_NORMAL_MODE; // Default mode
	
	public static void setDrivingModeSpeed(String mode)
	{
		if(AutoUtils.SPORT.equals(mode))
		{
			AutoUtils.TRAFFIC_SPEED = 5;  AutoUtils.EMERGENCY_TURBO_SPEED = 30; AutoUtils.SPEED_LIMIT_SIGN = 5;
		} else if(SAFE.equals(mode))
		{
			AutoUtils.TRAFFIC_SPEED = 15; AutoUtils.EMERGENCY_TURBO_SPEED = 30; AutoUtils.SPEED_LIMIT_SIGN = 5;
		}
	}
}

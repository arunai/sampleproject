package com.java.testprograms;

public class SlipperyRaodEvent implements Event {

	@Override
	public void applyEvent() {
		AutoUtils.currentSpeed -= AutoUtils.SPLIPPERY_RD_SPEED;
		AutoUtils.appliedProps.put(AutoUtils.SLIPPERY_ROAD_TYPE,true);		
	}
}

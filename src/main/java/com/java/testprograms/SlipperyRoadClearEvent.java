package com.java.testprograms;

public class SlipperyRoadClearEvent implements Event {

	@Override
	public void applyEvent() {
		if(AutoUtils.appliedProps.containsKey(AutoUtils.SLIPPERY_ROAD_TYPE))
		{
			AutoUtils.currentSpeed += AutoUtils.SPLIPPERY_RD_SPEED;
			AutoUtils.appliedProps.remove(AutoUtils.SLIPPERY_ROAD_TYPE);
		}
	}
}

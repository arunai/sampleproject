package com.java.testprograms;

public class TrafficEvent implements Event {

	@Override
	public void applyEvent() {
		AutoUtils.currentSpeed -= AutoUtils.TRAFFIC_SPEED;
		AutoUtils.appliedProps.put(AutoUtils.TRAFFIC_TYPE,true);		
	}
	
	

}

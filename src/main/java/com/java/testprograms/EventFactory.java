package com.java.testprograms;

public class EventFactory {

	public Event getEvent(int eventType) {

		switch (eventType) {
		case AutoUtils.TRAFFIC_TYPE:
			return new TrafficEvent();
		case AutoUtils.TRAFFIC_CLEAR_TYPE:
			return new TrafficClearEvent();
		case AutoUtils.WEATHER_RAINY_TYPE:
			return new WeatherEvent();
		case AutoUtils.WEATHER_CLEAR_TYPE:
			return new WeatherClearEvent();
		case AutoUtils.SLIPPERY_ROAD_TYPE:
			return new SlipperyRaodEvent();
		case AutoUtils.SLIPPERY_ROAD_CLEAR_TYPE:
			return new SlipperyRoadClearEvent();
		case AutoUtils.EMERGENCY_TURBO_TYPE:
			return new EmergencyTurboEvent();
		default:
			if (eventType < AutoUtils.MIN_SPEED_LIMIT || eventType > AutoUtils.MAX_SPEED_LIMIT) {
				System.out.println(AutoUtils.ERROR_INVALID_EVENT);
				return null;
			} else {
				return new SpeedLimitSignEvent(eventType);
			}
		}

	}
}

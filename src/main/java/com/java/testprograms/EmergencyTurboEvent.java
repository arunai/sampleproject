package com.java.testprograms;

public class EmergencyTurboEvent implements Event {

	@Override
	public void applyEvent() {
		if(!AutoUtils.appliedProps.containsKey(AutoUtils.SLIPPERY_ROAD_TYPE))
		{
			AutoUtils.currentSpeed += AutoUtils.EMERGENCY_TURBO_SPEED;
			AutoUtils.appliedProps.put(AutoUtils.EMERGENCY_TURBO_TYPE,true);
		}
		
	}

}

package com.java.testprograms;

import java.util.Scanner;

public class AutonomousDrivingDemo {

		
		public static void main(String args[])
		{
			
			int inputEvent;
			if(args.length > 0)
				AutoUtils.mode = args[0]; //Overwrite with given mode.
			AutoUtils.setDrivingModeSpeed(AutoUtils.mode);
			System.out.println(AutoUtils.WELCOME_MSG + AutoUtils.mode);
			do {
				System.out.println(AutoUtils.SPEED_IS + AutoUtils.currentSpeed);
				System.out.print(AutoUtils.ENTER_EVENT);
				Scanner scan = new Scanner(System.in);
				inputEvent = scan.nextInt();		
				calculateSpeed(inputEvent);
			}
			while(true);
			
		}
		
		private static void calculateSpeed(int inputEvent)
		{
			Boolean isEventAllowed = (Boolean)AutoUtils.appliedProps.get(inputEvent);
			if(AutoUtils.appliedProps.containsKey(inputEvent) && isEventAllowed) 
			{
				AutoUtils.appliedProps.put(inputEvent,false); // As this is second time, turn isEventAllowed flag to false and return to ignore the event.
				return;
			}
			
			EventFactory eventFactory = new EventFactory();
			Event event = eventFactory.getEvent(inputEvent); 
			if(null != event)
				event.applyEvent();
			
			if(AutoUtils.currentSpeed < AutoUtils.MIN_SPEED_LIMIT)
				AutoUtils.currentSpeed = AutoUtils.MIN_SPEED_LIMIT;
		}
}

package com.java.testprograms;

public class MyHelloTest {

	private String name;
	private static String staticStr =  "STATIC-STRING";
	 public MyHelloTest(String n)
	 {
		 this.name =n;
		 System.out.println(name);
		 testStaticMethod();
	 }
	 public static void testStaticMethod() {
		 System.out.println("Hey, I am in static method");
		 System.out.println(MyHelloTest.staticStr);
		 //System.out.println(name);
	 }
	 
	 public void testObjectMethod(){
	        System.out.println("Hey i am in non-static method");
	        //you can also call static variables here
	        System.out.println(MyHelloTest.staticStr);
	        //you can call instance variables here
	        System.out.println("Name: "+this.name);
	    }
	public static void main(String[] args) {
		System.out.println("Hello there");
		//MyHelloTest obj = new MyHelloTest();
		MyHelloTest obj1 =new MyHelloTest("Aruna");
		obj1.testStaticMethod();
	}

}

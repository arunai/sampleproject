package com.java.testprograms;

public interface Event {

	public void applyEvent();
	
}

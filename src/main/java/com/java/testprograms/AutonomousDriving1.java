package com.java.testprograms;

import java.util.Scanner;

public class AutonomousDriving1 {
	static int currentSpeed=20,isTraffic=0 , isWeather=0 , isSlippery=0 , emergencyTurbo=0 , speedLimit=0;
	public static void main(String[] args) {
		
		String mode = "NORMAL";
		if(args.length > 0)
			mode = args[0];
		System.out.println("Welcome. Driving mode is "+mode);
		System.out.println("Speed is "+ currentSpeed);
		System.out.println("Enter Event :");
		Scanner scan = new Scanner(System.in);
		processEvent(scan.nextInt());
	}
	
	public static void processEvent(int inputEvent)
	{
		calculateSpeed(inputEvent);
		System.out.println("Speed is "+ currentSpeed);
		System.out.print("Enter Event: ");
		Scanner scan = new Scanner(System.in);
		processEvent(scan.nextInt());
	}
	
	public static int calculateSpeed(int inputEvent) {

		
		switch(inputEvent) {
		case 1:
			isTraffic  = calcTraffic(true);
			break;
		case 2 :
			isTraffic  = calcTraffic(false);
			break;
		case 3 :
			isWeather = calcWeather(true);
			break;
		case 4 :
			isWeather = calcWeather(false);
			break;
		case 5 :
			isSlippery = calcSlipperyRd(true);
			break;
		case 6 :
			isSlippery = calcSlipperyRd(false);
			break;
		case 7 :
			emergencyTurbo = calcEmrTurbo(isSlippery);
			break;
		default :
			speedLimit = inputEvent;
		}
		currentSpeed = isTraffic + isWeather + isSlippery + emergencyTurbo + speedLimit;
		if(currentSpeed <= 10)
			currentSpeed = 10;
		return currentSpeed;
	}
	
	public static int calcTraffic(boolean isExists) {
		if(isExists && 20 <= isTraffic)
		{
				isTraffic = isTraffic -10;
		}
		else if(isTraffic == 10) {
			isTraffic = +10;
		}
		return isTraffic;
	}
	
	public static int calcWeather(boolean isExists) {
		if(isExists && isWeather == 5 )
		
			isWeather = -5;
		else 
			isWeather = +5;
		return isWeather;
	}
	
	public static int calcSlipperyRd(boolean isExists) {
		if(isExists )
			isSlippery = -15;
		else 
			isSlippery = +15;
		return isSlippery;
	}
	public static int calcEmrTurbo(int isSlipperyRd) {
		int EmrTurbo=0;
		if(isSlipperyRd > 0 ) // if slippery road then value will be -ve. 
			EmrTurbo = 20;
		
		return EmrTurbo;
	}
}

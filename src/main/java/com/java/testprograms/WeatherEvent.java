package com.java.testprograms;

public class WeatherEvent implements Event {

	@Override
	public void applyEvent() {
		AutoUtils.currentSpeed -= AutoUtils.WEATHER_RAINY_SPEED;
		AutoUtils.appliedProps.put(AutoUtils.WEATHER_RAINY_TYPE, true);
		
	}
}

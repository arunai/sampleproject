package com.java.testprograms;

import java.util.Properties;
import java.util.Scanner;

public class AutonomousDriving {

	public static final int TRAFFIC_TYPE = 1 , TRAFFIC_CLEAR_TYPE = 2, WEATHER_RAINY_TYPE = 3, WEATHER_CLEAR_TYPE = 4, SLIPPERY_ROAD_TYPE = 5, SLIPPERY_ROAD_CLEAR_TYPE = 6, EMERGENCY_TURBO_TYPE = 7;
	public static int INITIAL_SPEED = 20, MIN_SPEED_LIMIT = 10, MAX_SPEED_LIMIT = 100, TRAFFIC_SPEED = 10, WEATHER_RAINY_SPEED = 5, SPLIPPERY_RD_SPEED = 15, EMERGENCY_TURBO_SPEED = 20, SPEED_LIMIT_SIGN = 10;
	public static final String ERROR_INVALID_EVENT = "Invalid Event !" , WELCOME_MSG = "Welcome. Driving mode is ", SPEED_IS = "Speed is ", ENTER_EVENT = "Enter Event :" , DEFAULT_NORMAL_MODE = "NORMAL",SPORT = "Sport",SAFE ="Safe";
	public static int noOfTimesTrafficApplied;
	private static Properties appliedProps = new Properties();
	public static int currentSpeed = INITIAL_SPEED;
	public static String mode = DEFAULT_NORMAL_MODE; // Default mode
	
	public static void main(String[] args)
	{
		
		int inputEvent;
		if(args.length > 0)
			mode = args[0]; //Overwrite with given mode.
		setDrivingModeSpeed(mode);
		System.out.println(WELCOME_MSG + mode);
		do {
			System.out.println(SPEED_IS + currentSpeed);
			System.out.print(ENTER_EVENT);
			Scanner scan = new Scanner(System.in);
			inputEvent = scan.nextInt();		
			calculateSpeed(inputEvent);
		}
		while(true);
	}
	
	private static void setDrivingModeSpeed(String mode)
	{
		if(SPORT.equals(mode))
		{
			TRAFFIC_SPEED = 5;  EMERGENCY_TURBO_SPEED = 30; SPEED_LIMIT_SIGN = 5;
		} else if(SAFE.equals(mode))
		{
			TRAFFIC_SPEED = 15; EMERGENCY_TURBO_SPEED = 30; SPEED_LIMIT_SIGN = 5;
		}
	}
	
	private static void calculateSpeed(int inputEvent)
	{
		Boolean isEventAllowed = (Boolean)appliedProps.get(inputEvent);
		if(appliedProps.containsKey(inputEvent) && isEventAllowed) 
		{
			appliedProps.put(inputEvent,false); // As this is second time, turn isEventAllowed flag to false and return to ignore the event.
			return;
		}
		
		switch (inputEvent)
		{
			case TRAFFIC_TYPE :
					currentSpeed = currentSpeed - TRAFFIC_SPEED;
					appliedProps.put(TRAFFIC_TYPE,true);
				break;
			case TRAFFIC_CLEAR_TYPE :
				
				if(appliedProps.containsKey(TRAFFIC_TYPE))
				{
					currentSpeed = currentSpeed + TRAFFIC_SPEED;
					appliedProps.remove(TRAFFIC_TYPE);
				}
				break;
			case WEATHER_RAINY_TYPE :
					currentSpeed = currentSpeed - WEATHER_RAINY_SPEED;
					appliedProps.put(WEATHER_RAINY_TYPE, true);
				break;
			case WEATHER_CLEAR_TYPE : 
				if(appliedProps.containsKey(WEATHER_RAINY_TYPE))
				{
					currentSpeed = currentSpeed + WEATHER_RAINY_SPEED;
					appliedProps.remove(WEATHER_RAINY_TYPE);
				}
				break;
			case SLIPPERY_ROAD_TYPE :
					currentSpeed = currentSpeed - SPLIPPERY_RD_SPEED;
					appliedProps.put(SLIPPERY_ROAD_TYPE,true);
				break;
			case SLIPPERY_ROAD_CLEAR_TYPE :
				if(appliedProps.containsKey(SLIPPERY_ROAD_TYPE))
				{
					currentSpeed = currentSpeed + SPLIPPERY_RD_SPEED;
					appliedProps.remove(SLIPPERY_ROAD_TYPE);
				}
				break;
			case EMERGENCY_TURBO_TYPE :
				if(!appliedProps.containsKey(SLIPPERY_ROAD_TYPE))
				{
					currentSpeed = currentSpeed + EMERGENCY_TURBO_SPEED;
					appliedProps.put(EMERGENCY_TURBO_TYPE,true);
				}
				break;
			default :
				if(inputEvent < MIN_SPEED_LIMIT || inputEvent > MAX_SPEED_LIMIT)
				{
					System.out.println(ERROR_INVALID_EVENT);
				} else {
					setSpeedLimitSign(inputEvent);
				}
				break;
				
		}
		
		if(currentSpeed < MIN_SPEED_LIMIT)
			currentSpeed = MIN_SPEED_LIMIT;
	}

	private static void setSpeedLimitSign(int inputEvent) {
		//Remove emergency turbo speed before applying posted speed limit.
		if(appliedProps.containsKey(EMERGENCY_TURBO_TYPE))
		{
			currentSpeed = currentSpeed - EMERGENCY_TURBO_SPEED;
		}
		if(SPORT.equals(mode))
			inputEvent += SPEED_LIMIT_SIGN;
		else if(SAFE.equals(mode))
			inputEvent -= SPEED_LIMIT_SIGN;
			
			currentSpeed = inputEvent;
		appliedProps.put(SPEED_LIMIT_SIGN,true);
	}
}

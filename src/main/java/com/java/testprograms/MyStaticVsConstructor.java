package com.java.testprograms;

public class MyStaticVsConstructor {

		static {
			System.out.println("First");
			System.out.println("second");
			System.out.println("third");
			
		}
	public MyStaticVsConstructor() {
		System.out.println("constructor");
	}
	public MyStaticVsConstructor(String n) {
		System.out.println(n);
		//System.out.println("constructor");
	}
	
	public static void main(String[] args) {
		MyStaticVsConstructor obj = new MyStaticVsConstructor("aruna");
		
	}

}

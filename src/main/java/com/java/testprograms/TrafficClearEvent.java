package com.java.testprograms;

public class TrafficClearEvent implements Event {

	@Override
	public void applyEvent() {
		if(AutoUtils.appliedProps.containsKey(AutoUtils.TRAFFIC_TYPE))
		{
			AutoUtils.currentSpeed = AutoUtils.currentSpeed + AutoUtils.TRAFFIC_SPEED;
			AutoUtils.appliedProps.remove(AutoUtils.TRAFFIC_TYPE);
		}	
	}
}
